<?php

require_once("Recycling.class.php");

class PlasticRecycling extends Recycling
{
    private array $availablePlastics;

    /**
     * PlasticRecycling constructor
     * 
     * @param string $type
     * @param array $availablePlastics
     * @param float $capacity
     */
    public function __construct(string $type, array $availablePlastics, float $capacity)
    {
        parent::__construct($type, $capacity);
        $this->availablePlastics = $availablePlastics;
    }

    /**
     * Get available plastics
     * 
     * @return array $plastics
     */
    public function getAvailablePlastics(): array
    {
        return $this->availablePlastics;
    }
}

<?php

require_once("Recycling.class.php");

class GlassRecycling extends Recycling
{
    private bool $order;

    /**
     * GlassRecycling constructor
     * 
     * @param string $type
     * @param float $capacity
     * @param bool $order
     */
    public function __construct(string $type, float $capacity, bool $order)
    {
        parent::__construct($type, $capacity);
        $this->order = $order;
    }

    /**
     * check if the instance has order
     * 
     * @return bool
     */
    public function hasOrder(): bool
    {
        return $this->order;
    }
}

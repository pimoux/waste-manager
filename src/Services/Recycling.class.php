<?php

require_once("Service.class.php");

class Recycling extends Service
{
    private float $capacity;

    /**
     * Recycling constructor
     * 
     * @param string $type
     * @param float $capacity
     */
    public function __construct(string $type, float $capacity)
    {
        parent::__construct($type);
        $this->capacity = $capacity;
    }

    /**
     * Get capacity
     * 
     * @return float capacity
     */
    public function getCapacity(): float
    {
        return $this->capacity;
    }

    /**
     * set capacity
     * 
     * @param float capacity
     */
    public function setCapacity(float $capacity): void
    {
        $this->capacity = $capacity;
    }
}

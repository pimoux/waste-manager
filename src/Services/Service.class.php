<?php

abstract class Service
{
    private string $type;

    /**
     * Service constructor
     * 
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * get type
     * 
     * @return string 
     */
    public function getType(): string
    {
        return $this->type;
    }
}

<?php

require_once("Service.class.php");

class Incinerator extends Service implements TotalCapacityInterface
{
    private int $lineOven;
    private int $lineCapacity;

    /**
     * Incinerator constructor
     * 
     * @param string $type
     * @param int $lineoven
     * @param int $linecapacity
     */
    public function __construct(string $type, int $lineOven, int $lineCapacity)
    {
        parent::__construct($type);
        $this->lineOven = $lineOven;
        $this->lineCapacity = $lineCapacity;
    }

    /**
     * Get line oven
     * 
     * @return int lines oven
     */
    public function getLineOven(): int
    {
        return $this->lineOven;
    }

    /**
     * Get line capacity
     * 
     * @return int line capacity
     */
    public function getLineCapacity(): int
    {
        return $this->lineCapacity;
    }

    /**
     * Get total capacity
     * 
     * @return float total capacity
     */
    public function getTotalCapacity(): float
    {
        return $this->lineOven * $this->lineCapacity;
    }
}

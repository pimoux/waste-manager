<?php

interface TotalCapacityInterface {
    public function getTotalCapacity(): float;
}
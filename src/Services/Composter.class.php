<?php

require_once("Service.class.php");

class Composter extends Service implements TotalCapacityInterface
{
    private int $placesNumber;
    private float $capacity;

    /**
     * Composter constructor
     * 
     * @param string $type
     * @param float $capacity
     * @param int $placesNumber
     */
    public function __construct(string $type, float $capacity, int $placesNumber)
    {
        parent::__construct($type);
        $this->placesNumber = $placesNumber;
        $this->capacity = $capacity;
    }

    /**
     * Get places number
     * 
     * @return int placesNumber
     */
    public function getPlacesNumber(): int
    {
        return $this->placesNumber;
    }

    /**
     * Get capacity
     * 
     * @return float capacity
     */
    public function getCapacity(): float
    {
        return $this->capacity;
    }

    /**
     * set capacity
     * 
     * @param float capacity
     */
    public function setCapacity(float $capacity): void {
        $this->capacity = $capacity;
    }

    /**
     * Get total capacity
     * 
     * @return float total capacity
     */
    public function getTotalCapacity(): float
    {
        return $this->placesNumber * $this->capacity;
    }
}

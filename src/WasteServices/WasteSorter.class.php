<?php

class WasteSorter
{
    private array $wastes;

    /**
     * WasteSorter constructor
     * 
     * @param Waste[] $wastes
     */
    public function __construct(array $wastes)
    {
        $this->wastes = $wastes;
    }

    /**
     * Group all the amount of wastes
     * 
     * @return array
     */
    public function getSortedWastes(): array
    {
        $total = [];
        $wastesType = array_unique(
            array_map(
                function ($waste) {
                    return $waste->getType();
                },
                $this->wastes
            )
        );
        for ($i = 0; $i < count($wastesType); $i++) {
            $type = $wastesType[$i];
            $totalAmounts = array_reduce(
                array_map(
                    function (Waste $waste) {
                        return $waste->getAmount();
                    },
                    array_filter(
                        $this->wastes,
                        function ($waste) use ($type) {
                            return $waste->getType() === $type;
                        }
                    )
                ),
                function ($current, $next) {
                    $current += $next;
                    return $current;
                }
            );
            array_push($total, [$type => $totalAmounts]);
        }
        return $total;
    }
}

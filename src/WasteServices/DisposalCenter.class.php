<?php

class DisposalCenter
{
    private array $sortedWastes;
    private array $services;

    /**
     * WasteDisposalCenter constructor
     * 
     * @param array $sortedWastes
     * @param Service[] $services
     */
    public function __construct(array $sortedWastes, array $services)
    {
        $this->sortedWastes = $sortedWastes;
        $this->services = $services;
        $this->recycledWastes = [
            $this->compost(),
            $this->recycle(),
            $this->recyclePlastics()
        ];
        $this->incineratedWastes = $this->incinerate();
    }

    /**
     * Filter the sorted wastes by service
     * 
     * @param string $serviceType
     * 
     * @return array
     */
    public function applyFilterToServices(string $serviceType): array
    {
        $data = $this->sortedWastes;
        $filteredData = [];
        for ($i = 0; $i < count($data); $i++) {
            $type = array_keys($data[$i])[0];
            count($data[$i][$type]) > 0 && $data[$i][$type][0] instanceof $serviceType &&
                array_push($filteredData, $data[$i]);
        }
        return $filteredData;
    }

    /**
     * Empty waste thanks to services
     * 
     * @param array $data
     * 
     * @return array
     */
    public function emptyWastes(array $data): array
    {
        for ($i = 0; $i < count($data); $i++) {
            $type = array_keys($data[$i])[0];
            $treatedWastes = [];
            $isProcessEnded = false;
            for ($j = 0; $j < count($data[$i][$type]) && !$isProcessEnded; $j++) {
                $currentService = $data[$i][$type][$j];
                $capacity = $currentService instanceof TotalCapacityInterface ?
                    $currentService->getTotalCapacity() :
                    $currentService->getCapacity();
                if ($data[$i]["amount"] <= $capacity) {
                    array_push($treatedWastes, $data[$i]["amount"]);
                    $currentService->setCapacity($capacity - $data[$i]["amount"]);
                    $data[$i]["amount"] = 0;
                    $isProcessEnded = true;
                } else if (!$currentService instanceof PlasticRecycling) {
                    array_push($treatedWastes, $capacity);
                    $data[$i]["amount"] -= $capacity;
                    $currentService->setCapacity(0);
                }
            }
            $data[$i]["notIncinerate"] = $treatedWastes;
        }

        return $data;
    }

    /**
     * Empty plastics thanks to services
     * 
     * @param array $data
     * 
     * @return array
     */
    public function emptyPlastics(array $plastics): array
    {
        $services = $plastics[0]["plastiques"];
        $amounts = $plastics[0]["amount"];
        $treatedWastes = [];
        for ($i = 0; $i < count($amounts); $i++) {
            foreach ($amounts[$i] as $plasticType => $plasticAmount) {
                for ($j = 0; $j < count($services); $j++) {
                    if (in_array($plasticType, $services[$j]->getAvailablePlastics())) {
                        if ($plasticAmount <= $services[$j]->getCapacity()) {
                            if ($plasticAmount > 0) {
                                array_push($treatedWastes, [
                                    $plasticType => $plasticAmount,
                                    "serviceId" => $j + 1
                                ]);
                                $services[$j]->setCapacity($services[$j]->getCapacity() - $plasticAmount);
                                $plasticAmount = 0;
                            }
                        } else {
                            array_push($treatedWastes, [
                                $plasticType => $services[$j]->getCapacity(),
                                "serviceId" => $j + 1
                            ]);
                            $plasticAmount -= $services[$j]->getCapacity();
                            $services[$j]->setCapacity(0);
                        }
                    }
                }
            }
        }

        //1 remaining step: check if the plastics waste are completely empty and update the key amount
        $plastics[0]["plastiques"] = $services;
        $plastics[0]["notIncinerate"] = $treatedWastes;
        return $plastics;
    }

    /**
     * recycle wastes
     * 
     * @return array
     */
    public function recycle(): array
    {
        $filteredData = [];
        for ($i = 0; $i < count($this->sortedWastes); $i++) {
            $type = array_keys($this->sortedWastes[$i])[0];
            if (count($this->sortedWastes[$i][$type]) > 0) {
                if ($this->sortedWastes[$i][$type][0] instanceof Recycling && !$this->sortedWastes[$i][$type][0] instanceof PlasticRecycling) {
                    array_push($filteredData, $this->sortedWastes[$i]);
                }
            }
        }

        return $this->emptyWastes($filteredData);
    }

    /**
     * Recycle plastics
     * 
     * @return array
     */
    public function recyclePlastics(): array
    {
        $plastics = $this->applyFilterToServices("PlasticRecycling");

        return $this->emptyPlastics($plastics);
    }

    /**
     * Compost organic wastes
     * 
     * @return array
     */
    public function compost(): array
    {
        $organicWastes = $this->applyFilterToServices("Composter");

        return $this->emptyWastes($organicWastes);
    }

    /**
     * Incinerate the remaining wastes
     * 
     * @return array
     */
    public function incinerate(): array
    {
        $otherWastes = $this->sortedWastes[4];
        $incineratedWastes = [
            [array_keys($otherWastes)[0] => $otherWastes["amount"]]
        ];
        $incineratorsCapacities = array_map(
            function ($incinerator) {
                return $incinerator->getTotalCapacity();
            },
            array_filter($this->services, function ($service) {
                return $service instanceof Incinerator;
            })
        );

        for ($i = 0; $i < count($this->getRecycledWastes()); $i++) {
            for ($j = 0; $j < count($this->getRecycledWastes()[$i]); $j++) {
                $data = $this->getRecycledWastes()[$i][$j];
                $type = array_keys($data)[0];
                $amount = $data["amount"];
                if ($amount > 0 && !is_array($amount)) {
                    array_push($incineratedWastes, [$type => $amount]);
                }
            }
        }

        $result = [];

        for ($i = 0; $i < count($incineratedWastes); $i++) {
            $type = array_keys($incineratedWastes[$i])[0];
            for ($j = 0; $j < count($incineratorsCapacities); $j++) {
                if ($incineratedWastes[$i][$type] <= $incineratorsCapacities[$j]) {
                    if ($incineratedWastes[$i][$type] > 0) {
                        array_push($result, [
                            $type => $incineratedWastes[$i][$type],
                            "IncineratorId" => $j + 1
                        ]);
                        $incineratorsCapacities[$j] -= $incineratedWastes[$i][$type];
                        $incineratedWastes[$i][$type] = 0;
                    }
                } else if ($incineratorsCapacities[$j] > 0) {
                    array_push($result, [
                        $type => $incineratorsCapacities[$j],
                        "IncineratorId" => $j + 1
                    ]);
                    $incineratedWastes[$i][$type] -= $incineratorsCapacities[$j];
                    $incineratorsCapacities[$j] = 0;
                }
            }
        }

        return $result;
    }

    /**
     * get recycled wastes
     * 
     * @return array
     */
    public function getRecycledWastes(): array
    {
        return $this->recycledWastes;
    }

    /**
     * get incinerated wastes
     * 
     * @return array
     */
    public function getIncineratedWastes(): array
    {
        return $this->incineratedWastes;
    }
}

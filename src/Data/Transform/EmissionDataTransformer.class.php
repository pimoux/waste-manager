<?php

require_once("Emissions/Emission.class.php");
require_once("Emissions/RecyclingEmission.class.php");
require_once("Emissions/OrganicEmission.class.php");
require_once("DataTransformerInterface.php");

class EmissionDataTransformer implements DataTransformerInterface
{
    /**
     * Convert emission data into array of objects
     * 
     * @param array data
     * 
     * @return Emission[]
     */
    public static function transformData(array $data): array
    {
        $emissionObjects = [];
        foreach ($data as $type => $emission) {
            if ($type === "plastiques") {
                foreach ($data["plastiques"] as $plastic => $plasticEmission) {
                    $instance = self::createInstance([$plastic, $plasticEmission]);
                    if ($instance !== null) {
                        array_push($emissionObjects, $instance);
                    }
                }
            } else {
                $instance = self::createInstance([$type, $emission]);
                if ($instance !== null) {
                    array_push($emissionObjects, $instance);
                }
            }
        }
        
        return $emissionObjects;
    }

    /**
     * Create a emission instance depending on array of service
     * 
     * @param array emission
     * 
     * @return Emission|null
     */
    public static function createInstance(array $data): ?object
    {
        $className = "";
        if (array_key_exists("recyclage", $data[1])) {
            $className = "RecyclingEmission";
        } else if (array_key_exists("compostage", $data[1])) {
            $className = "OrganicEmission";
        } else if (array_key_exists("incineration", $data[1])) {
            $className = "Emission";
        }

        if ($className !== "") {
            $params = [$data[0]];
            foreach ($data[1] as $amount) {
                array_push($params, $amount);
            }
            $reflection_class = new ReflectionClass($className);
            return $reflection_class->newInstanceArgs($params);
        }

        return null;
    }
}

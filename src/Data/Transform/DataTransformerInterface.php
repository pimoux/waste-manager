<?php

interface DataTransformerInterface {
    public static function transformData(array $data): array;
    public static function createInstance(array $data): ?object;
}
<?php

require_once("Services/Recycling.class.php");
require_once("Services/Composter.class.php");
require_once("Services/GlassRecycling.class.php");
require_once("Services/PlasticRecycling.class.php");
require_once("Services/Incinerator.class.php");
require_once("DataTransformerInterface.php");

class ServiceDataTransformer implements DataTransformerInterface
{
    /**
     * Convert service data into array of objects
     * 
     * @param array data
     * 
     * @return Service[]
     */
    public static function transformData(array $data): array
    {
        $serviceObjects = [];
        for ($i = 0; $i < count($data); $i++) {
            $instance = self::createInstance($data[$i]);
            if ($instance !== null) {
                array_push($serviceObjects, $instance);
            }
        }
        
        return $serviceObjects;
    }

    /**
     * Create a service instance depending on array of service
     * 
     * @param array service
     * 
     * @return Service|null
     */
    public static function createInstance(array $data): ?object
    {
        $servicesWithAssociatedClasses = [
            "incinerateur" => "Incinerator",
            "recyclagePlastique" => "PlasticRecycling",
            "recyclagePapier" => "Recycling",
            "recyclageMetaux" => "Recycling",
            "recyclageVerre" => "GlassRecycling",
            "composteur" => "Composter"
        ];
        if ($servicesWithAssociatedClasses[$data["type"]] !== null) {
            $className = $servicesWithAssociatedClasses[$data["type"]];
            $reflection_class = new ReflectionClass($className);
            return $reflection_class->newInstanceArgs(array_values($data));
        }

        return null;
    }
}

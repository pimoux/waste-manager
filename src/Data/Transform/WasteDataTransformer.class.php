<?php

require_once("Wastes/Waste.class.php");
require_once("Wastes/Plastic.class.php");
require_once("Wastes/PaperWaste.class.php");
require_once("Wastes/OtherWaste.class.php");
require_once("Wastes/OrganicWaste.class.php");
require_once("Wastes/MetalWaste.class.php");
require_once("Wastes/GlassWaste.class.php");
require_once("Wastes/Plastics/PET.class.php");
require_once("Wastes/Plastics/PVC.class.php");
require_once("Wastes/Plastics/PC.class.php");
require_once("Wastes/Plastics/PEHD.class.php");
require_once("DataTransformerInterface.php");

class WasteDataTransformer implements DataTransformerInterface
{
    /**
     * Convert waste data into array of objects
     * 
     * @param array data
     * 
     * @return Waste[]
     */
    public static function transformData(array $data): array
    {
        $wasteObjects = [];
        for ($i = 0; $i < count($data); $i++) {
            foreach ($data[$i] as $type => $amount) {
                if ($type !== "population") {
                    if ($type !== "plastiques") {
                        $instance = self::createInstance([$type, $amount]);
                        if ($instance !== null) {
                            array_push($wasteObjects, $instance);
                        }
                    } else {
                        foreach ($amount as $plasticType => $plasticAmount) {
                            $instance = self::createInstance([$plasticType, $plasticAmount]);
                            if ($instance !== null) {
                                array_push($wasteObjects, $instance);
                            }
                        }
                    }
                }
            }
        }

        return $wasteObjects;
    }

    /**
     * Create a waste instance depending on array of waste
     * 
     * @param array waste
     * 
     * @return Waste|null
     */
    public static function createInstance(array $data): ?object
    {
        $typesWithAssociatedClass = [
            "verre" => "GlassWaste",
            "metaux" => "MetalWaste",
            "papier" => "PaperWaste",
            "organique" => "OrganicWaste",
            "autre" => "OtherWaste",
            "verre" => "Glasswaste",
            "PET" => "PET",
            "PVC" => "PVC",
            "PC" => "PC",
            "PEHD" => "PEHD"
        ];
        if ($typesWithAssociatedClass[$data[0]] !== null) {
            $className = $typesWithAssociatedClass[$data[0]];
            $reflection_class = new ReflectionClass($className);
            return $reflection_class->newInstanceArgs([$data[1]]);
        }

        return null;
    }
}

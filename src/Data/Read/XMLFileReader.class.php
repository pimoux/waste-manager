<?php

require_once("FileReader.class.php");

use Code\Exceptions\FileNotFoundException;

class XMLFileReader extends FileReader
{
    /**
     * XMLFileReader constructor
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     */
    public function __construct(string $wasteUrl, string $emissionUrl)
    {
        if (!file_exists($wasteUrl) || !file_exists($emissionUrl)) {
            throw new FileNotFoundException("file(s) not found");
        } else {
            parent::__construct("Xml");
            $data = $this->readFile($wasteUrl, $emissionUrl);
            $this->emissions = EmissionDataTransformer::transformData($data[1]);
            $this->wastes =  WasteDataTransformer::transformData($data[0]["quartiers"]);
            $this->services =  ServiceDataTransformer::transformData($data[0]["services"]);
        }
    }

    /**
     * Read XML files
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     * @return array
     */
    public function readFile(string $wasteUrl, string $emissionUrl): array
    {
        $wasteXmlObject = simplexml_load_file($wasteUrl);
        $wasteEncodedXml = json_encode($wasteXmlObject);
        $wastesJson = json_decode($wasteEncodedXml, true);

        $emissionXmlObject = simplexml_load_file($emissionUrl);
        $emissionEncodedXml = json_encode($emissionXmlObject);
        $emissionsJson = json_decode($emissionEncodedXml, true);

        return [
            $this->convertTypesInWasteAndServicesData($wastesJson),
            $this->convertTypesInEmissionData($emissionsJson)
        ];
    }

    /**
     * convert attributes to their corresponding types in waste and services data
     * 
     * @param array $wastesJson
     * 
     * @return array
     */
    private function convertTypesInWasteAndServicesData(array $wastesJson): array
    {
        foreach ($wastesJson as $key => $value) {
            for ($i = 0; $i < count($wastesJson[$key]); $i++) {
                foreach ($wastesJson[$key][$i] as $attribute => $value) {
                    if (!is_array($value)) {
                        if ($this->isfloat($value)) {
                            $wastesJson[$key][$i][$attribute] = floatval($value);
                        } else if (is_numeric($value)) {
                            $wastesJson[$key][$i][$attribute] = (int) $value;
                        } else if ($value === "true" || $value === "false") {
                            $wastesJson[$key][$i][$attribute] = $value === "true";
                        }
                    } else {
                        foreach ($wastesJson[$key][$i]["plastiques"] as $plastic => $plasticAmount) {
                            if ($this->isfloat($plasticAmount)) {
                                $wastesJson[$key][$i][$attribute][$plastic] = floatval($plasticAmount);
                            } else if (is_numeric($plasticAmount)) {
                                $wastesJson[$key][$i][$attribute][$plastic] = (int) $plasticAmount;
                            }
                        }
                    }
                }
            }
        }

        return $wastesJson;
    }

    /**
     * convert attributes to their corresponding types in emissions data
     * 
     * @param array $emissionsJson
     * 
     * @return array
     */
    private function convertTypesInEmissionData(array $emissionsJson): array
    {
        foreach ($emissionsJson as $emission => $value) {
            foreach ($emissionsJson[$emission] as $type => $amount) {
                if (!is_array($emissionsJson[$emission][$type])) {
                    $emissionsJson[$emission][$type] = (int) $amount;
                } else {
                    foreach ($emissionsJson[$emission][$type] as $plasticType => $plasticEmissions) {
                        $emissionsJson[$emission][$type][$plasticType] = (int) $plasticEmissions;
                    }
                }
            }
        }

        return $emissionsJson;
    }

    /**
     * Check if a string can be converted to float
     * 
     * @param string $num - string to check
     * 
     * @return bool 
     */
    private function isfloat(string $num): bool
    {
        return is_float($num) || is_numeric($num) && ((float) $num != (int) $num);
    }
}

<?php

require_once("FileReader.class.php");
use Code\Exceptions\FileNotFoundException;

class YAMLFileReader extends FileReader
{

    /**
     * YAMLFileReader constructor
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     */
    public function __construct(string $wasteUrl, string $emissionUrl)
    {
        if (!file_exists($wasteUrl) || !file_exists($emissionUrl)) {
            throw new FileNotFoundException("file(s) not found");
        } else {
            parent::__construct("Yaml");
            $data = $this->readFile($wasteUrl, $emissionUrl);
            // $this->emissions = EmissionDataTransformer::transformData($data[1]);
            // $this->wastes =  WasteDataTransformer::transformData($data[0]["quartiers"]);
            // $this->services =  ServiceDataTransformer::transformData($data[0]["services"]);
        }
    }

    /**
     * Read YAML files
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     * @return array
     */
    public function readFile(string $wasteUrl, string $emissionUrl): array
    {
        $test = file_get_contents($wasteUrl);
        $test2 = json_encode($test);
        $test3 = json_decode($test2, true);
        return [];
    }
}

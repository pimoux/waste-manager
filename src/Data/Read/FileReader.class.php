<?php

abstract class FileReader
{
    private string $name;
    protected array $services = [];
    protected array $wastes = [];
    protected array $emissions = [];

    /**
     * FileReader constructor
     * 
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->services = [];
        $this->wastes = [];
        $this->emissions = [];
    }

    /**
     * read files with different formats
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     * @return array
     */
    abstract public function readFile(string $wasteUrl, string $emissionUrl): array;

    /**
     * get services
     * 
     * @return Service[]
     */
    public function getServices(): array
    {
        return $this->services;
    }

    /**
     * get wastes
     * 
     * @return Waste[]
     */
    public function getWastes(): array
    {
        return $this->wastes;
    }

    /**
     * get emissions
     * 
     * @return Emission[]
     */
    public function getEmissions(): array
    {
        return $this->emissions;
    }

    /**
     * get name
     * 
     * @return string $name
     */
    public function getName(): string {
        return $this->name;
    }
}

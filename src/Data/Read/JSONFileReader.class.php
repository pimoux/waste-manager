<?php

require_once("../src/Data/Transform/WasteDataTransformer.class.php");
require_once("../src/Data/Transform/ServiceDataTransformer.class.php");
require_once("../src/Data/Transform/EmissionDataTransformer.class.php");
require_once("FileReader.class.php");
use Code\Exceptions\FileNotFoundException;

class JSONFileReader extends FileReader
{
    /**
     * JSONFileReader constructor
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     */
    public function __construct(string $wasteUrl, string $emissionUrl)
    {
        if (!file_exists($wasteUrl) || !file_exists($emissionUrl)) {
            throw new FileNotFoundException("file(s) not found");
        } else {
            parent::__construct("JSON");
            $data = $this->readFile($wasteUrl, $emissionUrl);
            $this->emissions = EmissionDataTransformer::transformData($data[1]);
            $this->wastes =  WasteDataTransformer::transformData($data[0]["quartiers"]);
            $this->services =  ServiceDataTransformer::transformData($data[0]["services"]);
        }
    }

    /**
     * Read JSON files
     * 
     * @param string wasteUrl
     * @param string emissionUrl
     * 
     * @return array
     */
    public function readFile(string $wasteUrl, string $emissionUrl): array
    {
        return [
            json_decode(file_get_contents($wasteUrl), true),
            json_decode(file_get_contents($emissionUrl), true)
        ];
    }
}

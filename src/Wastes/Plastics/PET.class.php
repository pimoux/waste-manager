<?php

require_once("../src/Wastes/Plastic.class.php");

class PET extends Plastic 
{
    /**
     * PET Constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("PET", $amount);
    }
}
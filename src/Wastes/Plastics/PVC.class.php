<?php

require_once("../src/Wastes/Plastic.class.php");

class PVC extends Plastic 
{
    /**
     * PVC Constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("PVC", $amount);
    }
}
<?php

require_once("../src/Wastes/Plastic.class.php");

class PC extends Plastic 
{
    /**
     * PC Constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("PC", $amount);
    }
}
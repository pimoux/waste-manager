<?php

require_once("../src/Wastes/Plastic.class.php");

class PEHD extends Plastic 
{
    /**
     * PEHD Constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("PEHD", $amount);
    }
}
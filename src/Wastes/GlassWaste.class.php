<?php

require_once("Waste.class.php");

class GlassWaste extends Waste
{
    /**
     * GlassWaste constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("verre", $amount);
    }
}
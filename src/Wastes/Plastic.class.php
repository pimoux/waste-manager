<?php

require_once("Waste.class.php");

class Plastic extends Waste 
{
    /**
     * Plastic constructor
     * 
     * @param string $type
     * @param float $amount
     */
    public function __construct(string $type, float $amount)
    {
        parent::__construct($type, $amount);
    }
}
<?php

abstract class Waste
{
    private string $type;
    private float $amount;

    /**
     * Waste constructor
     * 
     * @param string $type
     * @param float $amount
     */
    public function __construct(string $type, float $amount)
    {
        $this->type = $type;
        $this->amount = $amount;
    }

    /**
     * get amount
     * 
     * @return float $amount
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * get type
     * 
     * @return string $type
     */
    public function getType(): string
    {
        return $this->type;
    }
}

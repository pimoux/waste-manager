<?php

require_once("Waste.class.php");

class MetalWaste extends Waste
{
    /**
     * MetalWaste constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("metaux", $amount);
    }
}
<?php

require_once("Waste.class.php");

class OtherWaste extends Waste
{
    /**
     * OtherWaste constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("autre", $amount);
    }
}
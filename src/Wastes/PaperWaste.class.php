<?php

require_once("Waste.class.php");

class PaperWaste extends Waste
{
    /**
     * PaperWaste constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("papier", $amount);
    }
}
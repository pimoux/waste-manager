<?php

require_once("Waste.class.php");

class OrganicWaste extends Waste
{
    /**
     * OrganicWaste constructor
     * 
     * @param float $amount
     */
    public function __construct(float $amount)
    {
        parent::__construct("organique", $amount);
    }
}
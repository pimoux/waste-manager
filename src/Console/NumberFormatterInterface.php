<?php

interface NumberFormatterInterface {
    public function formatNumber(float $number): string;
}
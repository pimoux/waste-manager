<?php

require_once("Formatter.php");

class Printer
{
    private array $recycledWastes;
    private array $incineratedWastes;
    private array $emissions;
    private Formatter $formatter;

    const COLOR_RED = "1;31";
    const COLOR_GREEN = "1;32";
    const COLOR_YELLOW = "1;33";

    public function __construct(array $recycledWastes, array $incineratedWastes, array $emissions)
    {
        $this->recycledWastes = $recycledWastes;
        $this->incineratedWastes = $incineratedWastes;
        $this->emissions = $emissions;
        $this->formatter = new Formatter();
    }

    /**
     * Display treated wastes by services in the console
     */
    public function displayTreatedWasteInformations(): void
    {
        $informations = $this->recycledWastes;

        echo "\n" . "Voici la liste des déchets traités:" . "\n\n";
        for ($i = 0; $i < count($informations); $i++) {
            for ($j = 0; $j < count($informations[$i]); $j++) {
                $type = array_keys($informations[$i][$j])[0];
                if ($type !== "plastiques") {
                    $serviceNames = [
                        "papier" => "recyclage de papier",
                        "verre" => "recyclage de verre",
                        "organique" => "compost",
                        "metaux" => "recyclage de metaux",
                    ];
                    $totalNotIncinerated = array_reduce($informations[$i][$j]["notIncinerate"], function ($prev, $next) {
                        $prev += $next;
                        return $prev;
                    });
                    $incineratedWaste = $informations[$i][$j]["amount"];
                    $styledIncineratedWaste = $this->formatter->addColor(
                        $this->formatter->formatNumber($incineratedWaste),
                        self::COLOR_GREEN,
                        ["unit" => "kg"]
                    );
                    $styledTotalNotIncinerated = $this->formatter->addColor(
                        $this->formatter->formatNumber($totalNotIncinerated),
                        self::COLOR_GREEN,
                        ["unit" => "kg"]
                    );
                    $styledType = $this->formatter->addColor($type, self::COLOR_YELLOW);
                    echo "Les services de " . $serviceNames[$type] . " ont pu traiter " . $styledTotalNotIncinerated . " de " . $styledType . ", il reste à faire incinérer " . $styledIncineratedWaste . " de " . $styledType . ".\n";
                } else {
                    for ($k = 0; $k < count($informations[$i][$j]["notIncinerate"]); $k++) {
                        $currentTreatedPlastic = $informations[$i][$j]["notIncinerate"][$k];
                        $plasticType = array_keys($currentTreatedPlastic)[0];
                        $styledAmount = $this->formatter->addColor(
                            $this->formatter->formatNumber($currentTreatedPlastic[$plasticType]),
                            self::COLOR_GREEN,
                            ["unit" => "kg"]
                        );
                        $styledPlasticType = $this->formatter->addColor($plasticType, self::COLOR_YELLOW);
                        echo "Le recycleur de plastique numéro " . $currentTreatedPlastic["serviceId"] . " a pu traiter " . $styledAmount . " de " . $styledPlasticType . ".\n";
                    }
                }
            }
        }
    }

    /**
     * Display incinerated wastes in the console
     */
    public function displayIncineratedWasteInformations(): void
    {
        $remainingWastes = $this->incineratedWastes;
        echo "\n" . "Voici la liste des déchets incinérés: \n\n";
        for ($i = 0; $i < count($remainingWastes); $i++) {
            $type = array_keys($remainingWastes[$i])[0];
            $amount = $remainingWastes[$i][$type];
            $styledAmount = $this->formatter->addColor(
                $this->formatter->formatNumber($amount),
                self::COLOR_GREEN,
                ["unit" => "kg"]
            );
            $styledType = $this->formatter->addColor($type, self::COLOR_YELLOW);
            echo "L'incinérateur numéro " . $remainingWastes[$i]["IncineratorId"] . " a incinéré " . $styledAmount . " de " . $styledType . ".\n";
        }
    }

    /**
     * Display incinerated emissions in the console
     */
    public function displayIncineratedEmissions(): float
    {
        $remainingWastes = $this->incineratedWastes;
        $emissions = $this->emissions;
        $incineratedEmissions = [];
        $total = 0;

        echo "\nVoici les émissions de CO2 concernant les déchets incinérés: \n\n";

        for ($i = 0; $i < count($emissions); $i++) {
            $incineratedEmissions[$emissions[$i]->getType()] = $emissions[$i]->getIncineration();
        }

        for ($i = 0; $i < count($remainingWastes); $i++) {
            $type = array_keys($remainingWastes[$i])[0];
            $totalEmissions = $incineratedEmissions[$type] * $remainingWastes[$i][$type];
            $styledRemainingWastes = $this->formatter->addColor(
                $this->formatter->formatNumber($remainingWastes[$i][$type]),
                self::COLOR_GREEN,
                ["unit" => "kg"]
            );
            $styledTotalEmissions = $this->formatter->addColor(
                $this->formatter->formatNumber($totalEmissions),
                self::COLOR_RED,
                ["unit" => "kg"]
            );
            echo "Le CO2 émis par l'incinérateur numéro " . $remainingWastes[$i]["IncineratorId"] . ", ayant traité " . $styledRemainingWastes . " de " . $type . " est de " . $styledTotalEmissions . " de CO2.\n";
            $total += $totalEmissions;
        }

        $styledTotal = $this->formatter->addColor(
            $this->formatter->formatNumber($total),
            self::COLOR_RED,
            ["unit" => "kg"]
        );

        echo "Le total émis par incinération est de " . $styledTotal . ".\n";

        return $total;
    }

    public function displayTreatedEmissions(): float
    {

        $informations = $this->recycledWastes;
        $emissions = $this->emissions;
        $ecologicEmissions = [];
        $total = 0;

        for ($i = 0; $i < count($emissions); $i++) {
            if ($emissions[$i] instanceof RecyclingEmission) {
                /**
                 * @var RecyclingEmission $emission
                 */
                $emission = $emissions[$i];
                $ecologicEmissions[$emission->getType()] = $emission->getRecycling();
            } else if ($emissions[$i] instanceof OrganicEmission) {
                /**
                 * @var OrganicEmission $emission
                 */
                $emission = $emissions[$i];
                $ecologicEmissions[$emission->getType()] = $emission->getCompostage();
            }
        }

        echo "\nVoici les émissions de CO2 concernant les déchets traités de manière écologique: \n\n";

        for ($i = 0; $i < count($informations); $i++) {
            for ($j = 0; $j < count($informations[$i]); $j++) {
                $type = array_keys($informations[$i][$j])[0];
                if ($type !== "plastiques") {
                    $totalAmount = array_reduce($informations[$i][$j]["notIncinerate"], function ($prev, $next) {
                        $prev += $next;
                        return $prev;
                    });

                    $totalEmissions = $totalAmount * $ecologicEmissions[$type];
                    $total += $totalEmissions;
                    $styledTotalAmount = $this->formatter->addColor(
                        $this->formatter->formatNumber($totalAmount),
                        self::COLOR_GREEN, 
                        ["unit" => "kg"]
                    );
                    $styledTotalEmissions = $this->formatter->addColor(
                        $this->formatter->formatNumber($totalEmissions),
                        self::COLOR_RED, 
                        ["unit" => "kg"]
                    );
                    $styledType = $this->formatter->addColor($type, self::COLOR_YELLOW);
                    echo $styledTotalAmount . " de " . $styledType . " ont pu être traité de manière écologique. Avec " . $styledTotalAmount . " de " . $styledType . ", il y a " . $styledTotalEmissions . " de CO2 émis. \n";
                } else {
                    $totalPlasticAmounts = [];
                    for ($k = 0; $k < count($informations[$i][$j]["notIncinerate"]); $k++) {
                        $plasticType = array_keys($informations[$i][$j]["notIncinerate"][$k])[0];
                        $currentPlastic = $informations[$i][$j]["notIncinerate"][$k];
                        if (key_exists($plasticType, $totalPlasticAmounts)) {
                            $totalPlasticAmounts[$plasticType] += $currentPlastic[$plasticType];
                        } else {
                            $totalPlasticAmounts[$plasticType] = $currentPlastic[$plasticType];
                        }
                    }

                    foreach ($totalPlasticAmounts as $type => $amount) {
                        $totalEmissions = $amount * $ecologicEmissions[$type];
                        $total += $totalEmissions;
                        $styledTotalPlasticAmount = $this->formatter->addColor(
                            $this->formatter->formatNumber($totalPlasticAmounts[$type]),
                            self::COLOR_GREEN,
                            ["unit" => "kg"]
                        );
                        $styledType = $this->formatter->addColor($type, self::COLOR_YELLOW);
                        $styledTotalEmissions = $this->formatter->addColor(
                            $this->formatter->formatNumber($totalEmissions),
                            self::COLOR_RED,
                            ["unit" => "kg"]
                        );
                        echo $styledTotalPlasticAmount . " de " . $styledType . " ont pu être traité de manière écologique. Avec " . $styledTotalPlasticAmount . " de " . $styledType . ", il y a " . $styledTotalEmissions . " de CO2 émis." . "\n";
                    }
                }
            }
        }

        $styledTotal = $this->formatter->addColor(
            $this->formatter->formatNumber($total),
            self::COLOR_RED,
            ["unit" => "kg"]
        );

        echo "Le total émis par moyens écologiques est de " . $styledTotal . ". \n";

        return $total;
    }

    public function display(): void
    {
        $this->displayTreatedWasteInformations();
        $this->displayIncineratedWasteInformations();
        $treatedEmissions = $this->displayTreatedEmissions();
        $incineratedEmissions = $this->displayIncineratedEmissions();

        $styledTotal = $this->formatter->addColor(
            $this->formatter->formatNumber($treatedEmissions + $incineratedEmissions),
            self::COLOR_RED,
            ["unit" => "kg"]
        );

        echo "\n" . "Le total émis est donc de " . $styledTotal . "." . "\n";
    }
}

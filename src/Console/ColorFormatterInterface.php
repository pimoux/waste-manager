<?php

interface ColorFormatterInterface {
    public function addColor(string $text, string $color, array $options = []): string;
}
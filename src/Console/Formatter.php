<?php

require_once("NumberFormatterInterface.php");
require_once("ColorFormatterInterface.php");

class Formatter implements NumberFormatterInterface, ColorFormatterInterface {
    /**
     * Format number with french notation
     * @param float $number
     * @return string
     */
    public function formatNumber(float $number): string
    {
        return number_format($number, $number - (int) $number == 0 ? 0 : 2, ',', ' ');
    }

    public function addColor(string $text, string $color, array $options = []): string
    {
        if (key_exists("unit", $options)) {
            return "\e[" . $color . "m" . $text . $options["unit"] . "\e[0m";    
        }
        return "\e[" . $color . "m" . $text . "\e[0m";
    }
}
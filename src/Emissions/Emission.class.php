<?php

require_once("incineratorInterface.php");

class Emission implements IncineratorInterface
{
    private string $type;
    private int $incineration;

    /**
     * Emission constructor
     * 
     * @param string $type
     * @param int $incineration
     */
    public function __construct(string $type, int $incineration)
    {
        $this->type = $type;
        $this->incineration = $incineration;
    }

    /**
     * get type of emission
     * 
     * @return string type
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * get incineration of emission
     * 
     * @return int incineration
     */
    public function getIncineration(): int
    {
        return $this->incineration;
    }
}

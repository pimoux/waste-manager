<?php

require_once("Emission.class.php");
require_once("incineratorInterface.php");

class OrganicEmission extends Emission implements IncineratorInterface
{
    private int $compostage;

    /**
     * OrganicEmission constructor
     * 
     * @param string type
     * @param int compostage
     * @param int incineration
     */
    public function __construct(string $type, int $incineration, int $compostage)
    {
        parent::__construct($type, $incineration);
        $this->compostage = $compostage;
    }

    /**
     * get compostage
     * 
     * @return int compostage
     */
    public function getCompostage(): int
    {
        return $this->compostage;
    }
}

<?php

require_once("Emission.class.php");
require_once("incineratorInterface.php");

class RecyclingEmission extends Emission implements IncineratorInterface
{
    private int $recycling;

    /**
     * RecyclingEmission constructor
     * 
     * @param string type 
     * @param int incineration
     * @param int recycling
     */
    public function __construct(string $type, int $incineration, int $recycling)
    {
        parent::__construct($type, $incineration);
        $this->recycling = $recycling;
    }

    /**
     * get recycling
     * 
     * @return int recycling
     */
    public function getRecycling(): int
    {
        return $this->recycling;
    }
}

<?php

require_once('WasteManager.class.php');
require_once('Data/Read/JSONFileReader.class.php');
require_once('Data/Read/XMLFileReader.class.php');
require_once('Data/Read/YAMLFileReader.class.php');

//JSON

// $wasteManager = new WasteManager(new JSONFileReader('../Resources/Json/data.json', '../Resources/Json/co2.json'));
// $wasteManager->getInformations();

//XML

$wasteManager = new WasteManager(new XMLFileReader('../Resources/Xml/data.xml', '../Resources/Xml/co2.xml'));
$wasteManager->getInformations();
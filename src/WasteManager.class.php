<?php

require_once("Data/Read/FileReader.class.php");
require_once("WasteServices/WasteSorter.class.php");
require_once("Services/TotalCapacityInterface.php");
require_once("WasteServices/DisposalCenter.class.php");
require_once("Console/Printer.php");

class WasteManager
{
    private FileReader $fileReader;
    private WasteSorter $wasteSorter;
    private DisposalCenter $disposalCenter;
    public Printer $printer;

    /**
     * WasteManager constructor
     * 
     * @param FileReader $fileReader
     */
    public function __construct(FileReader $fileReader)
    {
        $this->fileReader = $fileReader;
        $this->wasteSorter = new WasteSorter($this->fileReader->getWastes());
        $this->disposalCenter = new DisposalCenter(
            $this->groupWasteAndServices(),
            $this->fileReader->getServices()
        );
        $this->printer = new Printer(
            $this->disposalCenter->getRecycledWastes(),
            $this->disposalCenter->incinerate(),
            $this->fileReader->getEmissions()
        );
    }

    /**
     * Gather wastes with their corresponding services
     * 
     * @return array
     */
    public function groupWasteAndServices(): array
    {
        $matches = [];
        $services = $this->fileReader->getServices();
        $wastesWithoutPlastics = array_values(
            array_filter($this->wasteSorter->getSortedWastes(), function ($waste) {
                return !in_array("PET", array_keys($waste)) &&
                    !in_array("PVC", array_keys($waste)) &&
                    !in_array("PC", array_keys($waste)) &&
                    !in_array("PEHD", array_keys($waste));
            })
        );
        $plastics = array_values(
            array_filter($this->wasteSorter->getSortedWastes(), function ($waste) {
                return in_array("PET", array_keys($waste)) ||
                    in_array("PVC", array_keys($waste)) ||
                    in_array("PC", array_keys($waste)) ||
                    in_array("PEHD", array_keys($waste));
            })
        );
        $plasticsRecycling = array_values(
            array_filter($services, function ($service) {
                return $service instanceof PlasticRecycling;
            })
        );

        for ($i = 0; $i < count($wastesWithoutPlastics); $i++) {
            foreach ($wastesWithoutPlastics[$i] as $type => $amount) {
                $matchingServices = array_values(
                    array_filter(
                        $services,
                        function ($service) use ($type) {
                            return $type === "organique" ? $service->getType() === "composteur" :
                                "recyclage" . strtolower($type) === strtolower($service->getType());
                        }
                    )
                );

                array_push($matches, [
                    $type => $matchingServices,
                    "amount" => $amount
                ]);
            }
        }
        array_push($matches, [
            "plastiques" => $plasticsRecycling,
            "amount" => $plastics
        ]);
        return $matches;
    }

    public function getInformations(): void
    {
        $this->printer->display();
    }
}
